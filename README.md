# SF-5 Skeleton
A skeleton for sf5-based projects.
The skeleton contains authentication system.

# Prerequisites
To run the project locally you need to have installed:
- php 8.0
- sqlite
- php-sqlite package and uncomment extensions ```extension=pdo_pgsql extension=pgsql``` in php.ini 
- git
- composer
- symfony (to start the server) 

# Installation
for the first installation do:

`git clone git@gitlab.com:lelysi/sf5-skeleton.git`

then, in a project run:

`composer install`

`php bin/console doctrine:schema:create`

you need to provide JWT_SECRET env variable
it is set by default for convenience
you can change it and store in `.env.local` file, or somewhere outside

# Usage

to start the server run:

`symfony server:start`

then you can do requests (examples in [httpie](https://httpie.org/) style):

- **registration:**

`http POST http://127.0.0.1:8000/auth/register \
email=test@example.com password=passwrd`

- **login:**

`http POST http://127.0.0.1:8000/auth/login \
email=test@example.com password=passwrd`

responds with jwt key for the user, the jwt key must be used in headers of next requests
assume that our jwt is
`eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.\
eyJ1c2VyIjoidGVzdEBleGFtcGxlLmNvbSIsImV4cCI6MTYwNjY3NDk1N30.ZaD7PCgnHnorDHqOuSBdEvmJnkNGUMPfSFvNhOdLXns`

## License
[MIT](https://choosealicense.com/licenses/mit/)
