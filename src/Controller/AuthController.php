<?php

declare(strict_types=1);

namespace App\Controller;

use App\JsonSchema\ValidationException;
use App\JsonSchema\Validator;
use App\Repository\UserRepository;
use Doctrine\ORM\ORMException;
use Firebase\JWT\JWT;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

final class AuthController extends BaseController
{
    /**
     * @throws ORMException
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder, UserRepository $userRepository): JsonResponse
    {
        try {
            $userData = $this->getValidatedUser($request);
        } catch (ValidationException $e) {
            return $this->error($e->getMessage());
        }

        $email = $userData->email;
        if ($userRepository->getByEmail($email) !== null) {
            return $this->error('User already exists');
        }

        $user = new User();
        $user->setPassword($encoder->encodePassword($user, $userData->password));
        $user->setEmail($email);
        $userRepository->storeUser($user);
        return $this->ok($user->getEmail(), HttpResponse::HTTP_CREATED);
    }


    public function login(Request $request, UserPasswordEncoderInterface $encoder, UserRepository $userRepository): JsonResponse
    {
        try {
            $userData = $this->getValidatedUser($request);
        } catch (ValidationException $e) {
            return $this->error($e->getMessage());
        }

        $user = $userRepository->getByEmail($userData->email);
        return ($user === null || !$encoder->isPasswordValid($user, $userData->password))
            ? $this->error('Email or password is wrong')
            : $this->json(
                [
                    'message' => 'success!',
                    'token' => sprintf('Bearer %s', $this->getJwt($user)),
                ]
            );
    }


    /**
     * @throws ValidationException
     */
    private function getValidatedUser(Request $request): object
    {
        return Validator::getValidatedObject($request->getContent(), 'user');
    }


    private function getJwt(User $user): string
    {
        return JWT::encode(
            [
                'user' => $user->getUsername(),
                'exp'  => (new \DateTime())->modify('+5 minutes')->getTimestamp(),
            ],
            $this->getParameter('jwt_secret'),
            $this->getParameter('jwt_algo')
        );
    }
}
