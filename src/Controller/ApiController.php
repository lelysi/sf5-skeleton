<?php

declare(strict_types=1);

namespace App\Controller;

use App\Security\JwtAuthenticator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final class ApiController extends BaseController
{
    public function test(Request $request, JwtAuthenticator $authenticator): JsonResponse
    {
        $user = $authenticator->getUserByRequest($request);
        return $user === null
            ? $this->error('Authentication Required')
            : $this->ok($user->getEmail() . '!');
    }
}
