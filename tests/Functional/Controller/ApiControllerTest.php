<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use App\Tests\Functional\BaseFunctionalTest;
use GuzzleHttp\Utils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ApiControllerTest extends BaseFunctionalTest
{
    public function testEndToEndAuth(): void
    {
        $testUserData = ['email' => 'test00@example.com', 'password' => '1232153'];

        $this->request('/auth/register', Request::METHOD_POST, $testUserData);
        self::assertSame(Response::HTTP_CREATED, $this->client->getResponse()->getStatusCode());

        $this->request('/auth/login', Request::METHOD_POST, $testUserData);
        self::assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $jwt = Utils::jsonDecode($this->client->getResponse()->getContent())->token;

        $this->request('/api/test', Request::METHOD_GET, [], ['HTTP_Authorization' => $jwt]);
        self::assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }
}